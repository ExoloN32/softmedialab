import { useDispatch } from "react-redux"
import { bindActionCreators } from "redux"
import ActionCriators from '../store/action-creators/'


export const useActions = () => {
    const dispatch = useDispatch()
    return bindActionCreators(ActionCriators, dispatch)
}