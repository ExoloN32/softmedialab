import React, {useRef, useEffect, useState} from 'react'
import axios from 'axios'

interface IModal {
    id: number,
    close: () => void,
    edit: (data: any) => void
}

const Modal: React.FC<IModal> = ({id, close, edit}) => {
    const name: any = useRef(null)
    const birthday: any = useRef(null)
    const skill: any = useRef(null)

    useEffect(() => {
        axios.get(`api/users/${id}`).then((resp) => {
            const user = resp.data
            name.current.value = user.name
            birthday.current.value = user.birthday
            skill.current.value = user.skill
        })
    }, [])

    const handleEdit = () => {
        const data = {
            id,
            name: name.current.value,
            birthday: birthday.current.value,
            skill: skill.current.value
        }
        edit(data)
    }

    return(
        <div className="modal">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="staticBackdropLabel">Modal title</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={close}></button>
                    </div>
                    <div className="modal-body">
                    <form className="container">
                        <input type="hidden" name="id" value="0" />
                        <div className="form-group">
                            <label>ФИО:
                                <input className="form-control" ref={name} />
                            </label>
                        </div>
                        <div className="form-group">
                            <label>Дата рождения:
                                <input className="form-control" type="date" ref={birthday} />
                            </label>
                        </div>
                        <label>Успеваемость:
                            <select className="form-control" ref={skill}>
                                <option value="неуд">неуд</option>
                                <option value="уд">уд</option>
                                <option selected value="хор">хор</option>
                                <option value="отл">отл</option>
                            </select>
                        </label>
                    </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" onClick={close}>Отмена</button>
                        <button type="button" className="btn btn-primary" onClick={handleEdit}>Изменить</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Modal