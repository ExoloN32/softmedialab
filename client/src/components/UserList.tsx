import React, {useEffect, useRef, useState} from 'react'
import { useActions } from '../hooks/useActions'
import { useTypedSelector } from '../hooks/useTypedSelector'
import Modal from './Modal'

interface IForm {
    id: number,
    name: string,
    birthday: string,
    skill: string
}

const UserList: React.FC = () => {
    const {users, error, loading} = useTypedSelector(state => state.user)
    const {getUsers, postUser, deleteUser, updateUser} = useActions()
    const name: any = useRef(null)
    const birthday: any = useRef(null)
    const skill: any = useRef(null)
    const [edit, setEdit] = useState(false)

    useEffect(() => {
        getUsers()
      }, [])

    const id = useRef(0)

    const handleSubmit = (event: any) => {
        event.preventDefault()
        const user = {
            name: name.current.value.replace(/ +/g, ' ').trim(),
            birthday: birthday.current.value,
            skill: skill.current.value
        }
        if(user.name === '' || user.birthday === '')
            return
        postUser(user, users)
    }

    const handleEdit = (event: any, _id: number) => {
        id.current = _id
        event.preventDefault()
        setEdit(true)
    }

    const handleDelete = (event: any, id: number) => {
        event.preventDefault()
        deleteUser(id)
    }

    const close = (): void => {
        setEdit(false)
    }

    const editFn = (data: IForm) => {
        setEdit(false)
        updateUser(data, users)
    }

    if(loading) {
        return <h1>Загрузка...</h1>
      }
    
      if(error) {
        return <h1>{error}</h1>
      }

    return(
        <div>
            {edit && <Modal id={id.current} close={close} edit={editFn}></Modal>}
            <h2>Список студентов</h2>
            <form className="form container" onSubmit={handleSubmit}>
                <input type="hidden" name="id" value="0" />
                <div className="form-group">
                    <label>ФИО:
                        <input className="form-control" ref={name} />
                    </label>
                </div>
                <div className="form-group">
                    <label>Дата рождения:
                        <input className="form-control" type="date" ref={birthday} />
                    </label>
                </div>
                <label>Успеваемость:
                    <select className="form-control" ref={skill}>
                        <option value="неуд">неуд</option>
                        <option value="уд">уд</option>
                        <option selected value="хор">хор</option>
                        <option value="отл">отл</option>
                    </select>
                </label>
                <div className="panel-body mt-3">
                    <button type="submit" className="btn btn-sm btn-primary">Добавить</button>
                </div>
            </form>
            <div className="container">
                <table className="table table-condensed table-striped table-bordered">
                    <thead><tr><th>Id</th><th>ФИО</th><th>Дата рождения</th><th>Успеваемость</th><th></th></tr></thead>
                    <tbody>
                    {users.map(user =>
                        <tr key={user.id}>
                            <td>{user.id}</td>
                            <td>{user.name}</td>
                            <td>{user.birthday}</td>
                            <td>{user.skill}</td>
                            <td>
                                <a className="lnk" onClick={e => handleEdit(e, user.id)}>Изменить</a>
                                <a className="lnk" onClick={e => handleDelete(e, user.id)}>Удалить</a>
                            </td>
                        </tr>
                    )}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default UserList