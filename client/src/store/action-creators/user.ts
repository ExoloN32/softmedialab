import { UserAction, UserActionTypes } from '../../types/user'
import { Dispatch } from "redux"
import axios from "axios"

export const getUsers = () => {
    return async (dispatch: Dispatch<UserAction>) => {
        try {
            dispatch({type: UserActionTypes.FETCH_USERS})
            const response = await axios.get('/api/users')
            dispatch({
                type: UserActionTypes.FETCH_USERS_SUCCESS,
                payload: response.data
            })
        } catch(e) {
            dispatch({
                type: UserActionTypes.FETCH_USERS_ERROR,
                payload: 'Произошла ошибка при загрузке студентов.'
            })
        }
    }
}

export const postUser = (user: any, users: any) => {
    return async (dispatch: Dispatch<UserAction>) => {
        try {
            dispatch({type: UserActionTypes.FETCH_USERS})
            const response = await axios.post('api/users', user)
            users.push(response.data)
            dispatch({
                type: UserActionTypes.FETCH_USERS_SUCCESS,
                payload: users
            })
        }
        catch(e) {
            dispatch({
                type: UserActionTypes.FETCH_USERS_ERROR,
                payload: 'Произошла ошибка при загрузке студентов.'
            })
        }
    }
}

export const deleteUser = (id: number) => {
    return async (dispatch: Dispatch<UserAction>) => {
        try {
            dispatch({type: UserActionTypes.FETCH_USERS})
            const response = await axios.delete(`/api/users/${id}`)
            dispatch({
                type: UserActionTypes.FETCH_USERS_SUCCESS,
                payload: response.data
            })
        } catch(e) {
            dispatch({
                type: UserActionTypes.FETCH_USERS_ERROR,
                payload: 'Произошла ошибка при загрузке студентов.'
            })
        }
    }
}

export const updateUser = (data: any, users: any) => {
    return async (dispatch: Dispatch<UserAction>) => {
        try {
            dispatch({type: UserActionTypes.FETCH_USERS});
            await axios.put('/api/users', data);
            users.forEach((el: any, ind: number) => {
                if(data.id === el.id) {
                    users[ind] = data
                }
            })
            dispatch({
                type: UserActionTypes.FETCH_USERS_SUCCESS,
                payload: users
            })
        } catch(e) {
            dispatch({
                type: UserActionTypes.FETCH_USERS_ERROR,
                payload: 'Произошла ошибка при загрузке студентов.'
            })
        }
    }
}
