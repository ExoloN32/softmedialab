import { Schema, model } from 'mongoose'

const testSchema = new Schema({
    id: {type: Number, required: true, unique: true},
    name: {type: String, required: true},
    birthday: {type: String, required: true},
    skill: {type: String, required: true},
}, {versionKey: false})

export default model('testUser', testSchema)