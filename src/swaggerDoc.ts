import swaggerUi from 'swagger-ui-express'
import swaggerJsdoc from 'swagger-jsdoc'
import fs from 'fs'

const swaggerDefinition = {
    openapi: '3.0.3',
    info: {
        title: "Test task",
        version: "1.0.0",
        description: ""
    },
    basePath: '/api'
}

const style = fs.readFileSync((process.cwd()+"/build/swagger.css"), 'utf8')

const cssOptions = {
    customCss: style
}

const options = {
     swaggerDefinition,
     apis: ['./build/routes/api.routes.js']
 }

const specs = swaggerJsdoc(options)

module.exports = (app: any) => {
    app.use('/api/docs', swaggerUi.serve, swaggerUi.setup(specs, cssOptions))
}