import { Router } from 'express'
import testUser from '../models/User'
const router = Router()
/**
 * @swagger
 * /users:
 *  get:
 *   description: This should return all users.
 */
router.get('/users', async (req, res) => {
    try {
        const users = await testUser.find({})
        if(!users) {
            return res.status(500).json({message: 'Не удалось получить данные.'})
        }
        res.status(200).json(users)
    }
    catch (e) {}
})
/**
 * @swagger
 * /users/:id:
 *  get:
 *   description: This should return the user by id.
 */
router.get('/users/:id', async (req, res) => {
    try {
        const id = req.params.id
        const user = await testUser.findOne({id})
        if(!user) {
            return res.status(400).json({message: 'Такого пользователя не существует.'})
        }
        res.status(200).json(user)
    }
    catch(error) {}
});
/**
 * @swagger
 * /users:
 *  post:
 *   description: This should add new user and return him whis id.
 */
router.post('/users', async (req, res) => {
    try {
        console.log('post', req.body)
        if(!req.body) {
            return res.status(400).json({message: 'Пустой запрос.'})
        }
        let id = 0
        const users = await testUser.find({})
        if (users.length > 0) {
            id = Math.max.apply(Math, users.map(function(o) {return o.id}))
        }
        ++id
        console.log('id', id)
        const obj = {...req.body, id}
        await testUser.create(obj, (err: any, doc: any) => {
            if(err) {
                console.log(err)
            }
            console.log(doc)
        })
        console.log(obj)
        res.status(200).json(obj)
    }
    catch(error) {}
})
/**
 * @swagger
 * /users/:id:
 *  delete:
 *   description: This should remove the user by id and return all users.
 */
router.delete('/users/:id', async (req, res) => {
    try {
        const id = req.params.id
        await testUser.deleteOne({id})
        const users = await testUser.find({})
        if(!users) {
            return res.status(500).json({message: 'Не удалось получить данные.'})
        }
        res.status(200).json(users)
    }
    catch(error) {}
});
/**
 * @swagger
 * /users:
 *  put:
 *   description: This should change the user by id.
 */
router.put('/users', async (req, res) => {
    try {
        if(!req.body) {
            return res.status(400).json({message: 'Пустой запрос.'})
        }
        await testUser.updateOne({id: req.body.id}, req.body);
        res.status(202).json({massage: 'Пользователь изменен.'})
    }
    catch(error) {}
});

module.exports = router