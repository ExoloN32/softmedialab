import express from'express'
import mongoose from 'mongoose'
import config from 'config'
import path from 'path'
import bodyParser from 'body-parser'
import fs from 'fs'
const swaggerDoc = require('./swaggerDoc')
fs.readFileSync((process.cwd()+"/build/swagger.css"), 'utf8')

const app = express()
const PORT = config.get('port') || 2000

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use('/api', require('./routes/api.routes'))

swaggerDoc(app)

app.use('/', express.static(path.join(__dirname, '..', 'client', 'build')))
app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '..', 'client', 'build', 'index.html'))
})



async function Start() {
    try{
        await mongoose.connect(config.get('mongoUri'), {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        })
        app.listen(PORT, () => console.log(`App has been started at port ${PORT}.`))
    }
    catch (error) {
        console.log('Server error', error.message)
        process.exit(1)
    }
}

Start()